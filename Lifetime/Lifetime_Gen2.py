import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import ast
import pyodbc

#SQL Server Stuff

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 13 for SQL Server}'

def delete():
    cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    with cursor.execute("IF OBJECT_ID('[Cleantech Meter Readings].dbo.Station_LTSummary', 'U') IS NOT NULL TRUNCATE TABLE [Cleantech Meter Readings].dbo.Station_LTSummary"): 
        print('Successfuly Deleted cause already present!') 
delete()      
#Cols needed are Date,O&MR Refrence,GHI,Last-R-MFM and Eac-MFM

tz = pytz.timezone('Asia/Calcutta')
path='/home/admin/Dropbox/Lifetime/Gen-1/'
path2='/home/admin/Dropbox/Lifetime/Gen-2/Station_LTSummary.txt'
#Initialising
columns=['Date','GHI','GHI-Flag','LastR-MFM','LastR-Flag','Eac-MFM','Eac-Flag','Reference','Station_Id']
cols=['Seris_Station_Name','Flexi_Station_Name','Locus_Station_Name','Seris_Meter_Reference','Flexi_Meter_Reference','Locus_Meter_Reference','Seris_No_Meters','Flexi_No_Meters','Locus_No_Meters']
stns=[]
meterref=[]
meterno=[]
df=pd.read_csv('/home/pranav/Lifetime.csv' )
for i in range(3):
    temp=df[cols[i]].dropna().tolist()
    for k in temp:
        temp2=k[1:7]+'-LT.txt'
        stns.append(temp2)
    temp=df[cols[i+3]].dropna().tolist()
    for j in temp:
        temp=ast.literal_eval(j)
        meterref.append(temp)
    temp=list(map(int,df[cols[i+6]].dropna().tolist()))
    for l in temp:
        meterno.append(l)

#Hardcoded
stns.append('SG-006-LT.txt')
meterref.append(['SG-006A','SG-006B','SG-006C','SG-006D','SG-006E'])
meterno.append(5)

if(os.path.exists(path2)):  #Historical
    pass
    print("PASS")
else:
    connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    for m,n in enumerate(stns):
        print(n)
        df=pd.read_csv(path+n,sep='\t')
        cols=df.columns.tolist()
        for i in range(meterno[m]):
            temp=[]
            temp2=[]
            temp=cols[0:3]
            temp.append(cols[3+2*i])
            temp.append(cols[4+2*i])
            temp.append(cols[3+meterno[m]*2+2*i])
            temp.append(cols[2+2*meterno[m]+(2*(i+1))])
            for temp3 in temp:
                temp2.append(temp3)
            df2=df[temp].copy()
            df2['MeterReference']=meterref[m][i]
            df2['Station_Id']=meterref[m][i][:-1]
            df2.columns=columns
            print(df2.columns)
            df2=df2.fillna(0)
            df2.loc[df2['Eac-MFM'] > 100000, 'Eac-MFM'] = 0
            df2.loc[df2['Eac-MFM'] < 0, 'Eac-MFM'] = 0
            cursor = connStr.cursor()
            for index,row in df2.iterrows():
                print(row)
                with cursor.execute("INSERT INTO dbo.Station_LTSummary([Date],[GHI],[GHI-Flag],[LastR-MFM],[LastR-Flag],[Eac-MFM],[Eac-Flag],[Reference],[Station_Id]) values (?, ?, ?, ?, ?, ?, ?, ?, ?)", row['Date'], row['GHI'], row['GHI-Flag'], row['LastR-MFM'], row['LastR-Flag'],row['Eac-MFM'], row['Eac-Flag'],row['Reference'],row['Station_Id']):
                    pass
                connStr.commit()
            cursor.close()
            if(m==0 and i==0):
                df2.to_csv(path2,sep='\t',mode='w',index=False)
            else:
                df2.to_csv(path2,sep='\t',mode='a',index=False,header=False)
    connStr.close()

oldtime=datetime.datetime.fromtimestamp(os.path.getmtime(path+'IN-052-LT.txt'))
while(1):   #Live
    newtime=datetime.datetime.fromtimestamp(os.path.getmtime(path+'IN-052-LT.txt'))
    if(newtime>oldtime):
        #Time setting
        connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
        timenow=datetime.datetime.now(tz)+datetime.timedelta(hours=-.1)
        timenowstr=str(timenow)
        timenowdate=str(timenow.date())
        print(timenowdate)
        df2=pd.read_csv(path2,sep='\t')
        #Reading
        for m,n in enumerate(stns):
            print(n)
            df=pd.read_csv(path+n,sep='\t')
            cols=df.columns.tolist()
            #Historical
            for i in range(1,15):
                pastdate=timenow+datetime.timedelta(days=-i)
                pastdatestr=str(pastdate)
                pastdatestr2=pastdate.strftime("%d-%m-%Y")
                if((((df2['Date']==pastdatestr[0:10]) & (df2['Reference']==meterref[m][0]))).any() or (((df2['Date']==pastdatestr2[0:10]) & (df2['Reference']==meterref[m][0]))).any()):
                    df3=df.loc[(df['Date']==pastdatestr[0:10]) | (df['Date']==pastdatestr2[0:10])]
                    for i in range(meterno[m]):
                        temp=[]
                        temp=cols[0:3]
                        temp.append(cols[3+2*i])
                        temp.append(cols[4+2*i])
                        temp.append(cols[3+meterno[m]*2+2*i])
                        temp.append(cols[2+2*meterno[m]+(2*(i+1))])
                        df4=df3[temp].copy()
                        df4['MeterReference']=meterref[m][i]
                        df4['Station_Id']=meterref[m][i][:-1]
                        df4=df4.fillna(0)
                        vals=df4.values.tolist()
                        cursor = connStr.cursor()
                        for index,row in df4.iterrows():
                            with cursor.execute("UPDATE [dbo].[Station_LTSummary] SET [GHI]="+str(vals[0][1])+",[GHI-Flag]="+str(vals[0][2])+",[LastR-MFM]="+str(vals[0][3])+",[LastR-Flag]="+str(vals[0][4])+",[Eac-MFM]="+str(vals[0][5])+",[Eac-Flag]="+str(vals[0][6])+" WHERE [Date]='"+str(vals[0][0])+"' and [Reference]='"+vals[0][7]+"'"):
                                pass
                            connStr.commit()
                        cursor.close()
                        df2.loc[(df2['Date']==pastdatestr[0:10]) & (df2['Reference']==meterref[m][i]),df2.columns.tolist()]=vals[0] 
                else:
                    print('ABSENT')
                    df3=df.loc[(df['Date']==pastdatestr[0:10]) | (df['Date']==pastdatestr2[0:10])]
                    if(df3.empty):
                        print('Data with Date not present!')
                    else:
                        print('jsdaasdjas')
                        print(meterno[m])
                        for i in range(meterno[m]):
                            temp=[]
                            temp=cols[0:3]
                            temp.append(cols[3+2*i])
                            temp.append(cols[4+2*i])
                            temp.append(cols[3+meterno[m]*2+2*i])
                            temp.append(cols[2+2*meterno[m]+(2*(i+1))])
                            df4=df3[temp].copy()
                            df4['MeterReference']=meterref[m][i]
                            df4['Station_Id']=meterref[m][i][:-1]
                            df4.columns=columns
                            df4=df4.fillna(0)
                            cursor = connStr.cursor()
                            for index,row in df4.iterrows():
                                with cursor.execute("INSERT INTO dbo.Station_LTSummary([Date],[GHI],[GHI-Flag],[LastR-MFM],[LastR-Flag],[Eac-MFM],[Eac-Flag],[Reference],[Station_Id]) values (?, ?, ?, ?, ?, ?, ?, ?, ?)", row['Date'], row['GHI'], row['GHI-Flag'], row['LastR-MFM'], row['LastR-Flag'],row['Eac-MFM'], row['Eac-Flag'],row['Reference'],row['Station_Id']):
                                    pass
                                connStr.commit()
                            cursor.close()
                            print(df4)
                            df2=pd.concat([df2, df4], axis=0)
                            df4.to_csv(path2,sep='\t',mode='a',index=False,header=False)
                    df2=pd.read_csv(path2,sep='\t')
            #Live
            if(((df2['Date']==timenowdate) & (df2['Reference']==meterref[m][0])).any()):#If already present in Gen-2
                print('PRESENT IN GEN 2!')
                df3=df.loc[df['Date']==timenowdate]
                for i in range(meterno[m]):
                    temp=[]
                    temp=cols[0:3]
                    temp.append(cols[3+2*i])
                    temp.append(cols[4+2*i])
                    temp.append(cols[3+meterno[m]*2+2*i])
                    temp.append(cols[2+2*meterno[m]+(2*(i+1))])
                    df4=df3[temp].copy()
                    df4['MeterReference']=meterref[m][i]
                    df4['Station_Id']=meterref[m][i][:-1]
                    df4=df4.fillna(0)
                    vals=df4.values.tolist()
                    cursor = connStr.cursor()
                    for index,row in df4.iterrows():
                        print(vals[0][0])
                        with cursor.execute("UPDATE [dbo].[Station_LTSummary] SET [GHI]="+str(vals[0][1])+",[GHI-Flag]="+str(vals[0][2])+",[LastR-MFM]="+str(vals[0][3])+",[LastR-Flag]="+str(vals[0][4])+",[Eac-MFM]="+str(vals[0][5])+",[Eac-Flag]="+str(vals[0][6])+" WHERE [Date]='"+str(vals[0][0])+"' and [Reference]='"+vals[0][7]+"'"):
                            pass
                        connStr.commit()
                    cursor.close()
                    print(vals)
                    df2.loc[(df2['Date']==timenowdate) & (df2['Reference']==meterref[m][i]),df2.columns.tolist()]=vals[0] 
                df2.to_csv(path2,sep='\t',mode='w',index=False,header=True)
            else:
                print('ABSENT IN GEN 2')
                print('jsjssj')
                df3=df.loc[df['Date']==timenowdate]
                for i in range(meterno[m]):
                    temp=[]
                    temp=cols[0:3]
                    temp.append(cols[3+2*i])
                    temp.append(cols[4+2*i])
                    temp.append(cols[3+meterno[m]*2+2*i])
                    temp.append(cols[2+2*meterno[m]+(2*(i+1))])
                    df4=df3[temp].copy()
                    df4['MeterReference']=meterref[m][i]
                    df4['Station_Id']=meterref[m][i][:-1]
                    df4.columns=columns
                    df4=df4.fillna(0)
                    cursor = connStr.cursor()
                    for index,row in df4.iterrows():
                        with cursor.execute("INSERT INTO dbo.Station_LTSummary([Date],[GHI],[GHI-Flag],[LastR-MFM],[LastR-Flag],[Eac-MFM],[Eac-Flag],[Reference],[Station_Id]) values (?, ?, ?, ?, ?, ?, ?, ?, ?)", row['Date'], row['GHI'], row['GHI-Flag'], row['LastR-MFM'], row['LastR-Flag'],row['Eac-MFM'], row['Eac-Flag'],row['Reference'],row['Station_Id']):
                            pass
                        connStr.commit()
                    cursor.close()
                    if(df4.empty):
                        print('Waiting')
                    else:
                        df2=pd.concat([df2, df4], axis=0)
                        df4.to_csv(path2,sep='\t',mode='a',index=False,header=False)
        print(df2.tail())
        print("Sleeping")
        connStr.close()
        oldtime=newtime
    else:
        print('OLD')
    time.sleep(300)



