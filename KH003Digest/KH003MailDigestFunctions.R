rm(list = ls())
TIMESTAMPSALARM = NULL
TIMESTAMPSALARM2 = NULL
TIMESTAMPSALARM3 = NULL
METERCALLED = 0
ltcutoff = .001
CABLOSSTOPRINT = 0
SOLENERGYLOSS = 0
instCap = 2547.2
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

getPRData = function(df)
{
	PR1=gsi=dsp=gsi2=PR2 = dsp2=stn=NA
	date = as.character(df[,1])
	yr = substr(date,1,4)
	mon=substr(date,1,7)
	date = substr(date,1,10)
	path = paste('/home/admin/Dropbox/Second Gen/[KH-003S]/',yr,"/",mon,"/[KH-003S] ",date,".txt",sep="")
	path2 = paste('/home/admin/Dropbox/Second Gen/[KH-714S]/',yr,"/",mon,"/[KH-714S] ",date,".txt",sep="")
	print(paste('Path reading GSI is from',path))
	if(file.exists(path2))
	{
		dataread = read.table(path2,header=T,sep="\t",stringsAsFactors=F)
		gsi2 = as.numeric(dataread[1,3])
		PR2 = format(round((as.numeric(df[,3]) *100/instCap/gsi2),1),nsmall=1)
		dsp2 = as.numeric(PR2)*instCap
	}
	if(file.exists(path))
	{
	print('File exists')
	dataread = read.table(path,header = T,sep = "\t",stringsAsFactors=F)
	{
		gsi = as.numeric(df[1,3])
	}
	gsi = as.numeric(dataread[1,4])
	PR1 = format(round((as.numeric(df[,3]) *100/instCap/gsi),1),nsmall=1)
	dsp = as.numeric(PR1)*instCap
	stn = "KH-003S"
	}
	{
	if(((!is.finite(gsi)) && (!is.finite(gsi2))) || ((gsi < 1) && (gsi2 < 1)))
	{
		PR1=70
		gsi = format(round((as.numeric(df[,3]) *100/instCap/PR1),1),nsmall=1)
		dsp = PR1 * instCap
		stn = "Artificial-synthesis"
	}
	else if(is.finite(gsi) && is.finite(gsi2))
	{
		diff = abs(gsi-gsi2)
		if(diff > (max(c(gsi,gsi2))*.5))
		{
			if(gsi2 > gsi)
			{
				gsi = gsi2
				PR1 = PR2
				dsp = dsp2
				stn = "KH-714S"
			}
		}
	}
	else if(is.finite(gsi2))
	{
				gsi = gsi2
				PR1 = PR2
				dsp = dsp2
				stn = "KH-714S"
	}
	}
	if(is.finite(PR1) && PR1 < 65)
		PR1 = 70
	array2 = c(dsp,PR1,gsi,stn,gsi2)
	return(array2)
}

secondGenData = function(filepath,writefilepath)
{
  {
		if(METERCALLED == 1){
	 		TIMESTAMPSALARM <<- NULL}
		else if(METERCALLED == 2){
			TIMESTAMPSALARM2 <<- NULL}
		else if(METERCALLED == 3)
		{
			TIMESTAMPALARM3 <<- NULL
		}
	}
	print(paste('IN 2G',filepath))
  dataread = read.table(filepath,header = T,sep = "\t",stringsAsFactors=F)
	dataread2 = dataread
	EacCol = 39
  PacCol = 15
	
	if(nrow(dataread))
	{
		date = as.character(dataread[1,1])
		yr = as.numeric(substr(date,1,4))
		mon = as.numeric(substr(date,6,7))
		day = as.numeric(substr(date,9,10))
		print(paste('Yr',yr,'Mon',mon,'Day',day))
		if(is.finite(yr) && is.finite(mon) && is.finite(day) && (yr > 2017 || (yr ==2017 && mon > 5) || (yr == 2017 && mon ==5 && day > 13)))
		{
			EacCol = 45
			PacCol = 18
		}
		if(is.finite(yr) && is.finite(mon) && is.finite(day) && (yr > 2018 || (yr == 2019 && mon ==1 && day > 10)))
		{
			EacCol = 46
			PacCol = 19
		}
	}

	dataread = dataread2[complete.cases(dataread2[,EacCol]),]
	lasttime = as.character(dataread[nrow(dataread),1])
	lastread = as.character(round(as.numeric(dataread[nrow(dataread),EacCol])/1000,3))
  Eac2 = format(round((as.numeric(dataread[nrow(dataread),EacCol]) - as.numeric(dataread[1,EacCol]))/1000,2),nsmall=1)
	dataread = dataread2[complete.cases(as.numeric(dataread2[,PacCol])),]
	dataread = dataread[as.numeric(dataread[,PacCol]) > 0,]
	Eac1 = 0
	if(nrow(dataread) > 1)
	{
   	Eac1 = format(round(sum(as.numeric(dataread[,PacCol]))/60000,1),nsmall=2)
	}
	RATIO = round(as.numeric(Eac1)*100/as.numeric(Eac2),1)
	dataread = dataread2
  DA = format(round(nrow(dataread)/14.4,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 480,]
  tdx = tdx[tdx > 480]
  dataread2 = dataread2[tdx < 1020,]
	dataread2 = dataread2[complete.cases(as.numeric(dataread2[,PacCol])),]
  missingfactor = 540 - nrow(dataread2)
  dataread2 = dataread2[as.numeric(dataread2[,PacCol]) < ltcutoff,]
	if(length(dataread2[,1]) > 0)
	{
		{
			if(METERCALLED == 1){
				TIMESTAMPSALARM <<- as.character(dataread2[,1])}
			else if(METERCALLED == 2){
		 	 TIMESTAMPSALARM2 <<- as.character(dataread2[,1])}
			else if(METERCALLED == 3){
		 	 TIMESTAMPSALARM3 <<- as.character(dataread2[,1])}
		}
	}
  totrowsmissing = format(round((missingfactor + nrow(dataread2))/5.4,1),nsmall=1)
	Yld1 = round(as.numeric(Eac1)/instCap,2)
	Yld2 = round(as.numeric(Eac2)/instCap,2)
	{
	  if(METERCALLED == 1)
	  {
      df = data.frame(Date = substr(as.character(dataread[1,1]),1,10), SolarPowerMeth1 = as.numeric(Eac1),SolarPowerMeth2 = as.numeric(Eac2),
                  Ratio=RATIO,DA = DA,Downtime = totrowsmissing,
									LastTime = lasttime, LastRead = lastread,stringsAsFactors = F)
			vals = getPRData(df)
			PR1 = round(as.numeric(Yld1)*100/as.numeric(vals[3]),1)
      df = data.frame(Date = substr(as.character(dataread[1,1]),1,10), SolarPowerMeth1 = as.numeric(Eac1),SolarPowerMeth2 = as.numeric(Eac2),
                  Ratio=RATIO,DA = DA,Downtime = totrowsmissing,
									LastTime = lasttime, LastRead = lastread,Yld = as.character(vals[1]),PR=as.character(vals[2]),
									Yld1 = Yld1, Yld2 = Yld2,
									IrradianceSource=vals[4],
									PR1=PR1,GSI=as.character(vals[3]),
									stringsAsFactors = F)
    } 
	  else if(METERCALLED == 2)
	  {
      df = data.frame(Date = substr(as.character(dataread[1,1]),1,10), CokeEnergyMeth1 = as.numeric(Eac1),CokeEnergyMeth2= as.numeric(Eac2),
                  Ratio=RATIO,DA = DA,Downtime = totrowsmissing,
									LastTime = lasttime, LastRead = lastread, stringsAsFactors = F)
			vals = getPRData(df)
			PR1 = round(as.numeric(Yld1)*100/as.numeric(vals[3]),1)
      df = data.frame(Date = substr(as.character(dataread[1,1]),1,10), CokeEnergyMeth1 = as.numeric(Eac1),CokeEnergyMeth2= as.numeric(Eac2),
                  Ratio=RATIO,DA = DA,Downtime = totrowsmissing,
									LastTime = lasttime, LastRead = lastread, Yld = as.character(vals[1]),PR=as.character(vals[2]),
									Yld1 = Yld1, Yld2 = Yld2, 
									IrradianceSource=vals[4],
									PR1=PR1,GSI=as.character(vals[3]),
									stringsAsFactors = F)
	  }
	  else if(METERCALLED == 3)
	  {
			DailyCons = dataread[,ncol(dataread)]
			DailyCons = DailyCons[complete.cases(DailyCons)]
			DailyCons = round(sum(DailyCons)/60000,2)
			ExpAmt = as.numeric(dataread[,PacCol])
			ExpAmt = ExpAmt[complete.cases(ExpAmt)]
			ExpAmtFinal = NA
			if(length(ExpAmt))
			{
				ExpAmt = ExpAmt[ExpAmt < 0]
				if(length(ExpAmt))
				{
					ExpAmt = round(sum(ExpAmt)/-60000,2)
					ExpAmtFinal = ExpAmt
				}
			}
      df = data.frame(Date = substr(as.character(dataread[1,1]),1,10), CokeEnergyMeth1 = as.numeric(Eac1),CokeEnergyMeth2= as.numeric(Eac2),
                  Ratio=RATIO,DA = DA,Downtime = totrowsmissing,
									LastTime = lasttime, LastRead = lastread, DailyCons = DailyCons,stringsAsFactors = F)
			vals = getPRData(df)
			PR1 = round(as.numeric(Yld1)*100/as.numeric(vals[3]),1)
      df = data.frame(Date = substr(as.character(dataread[1,1]),1,10), CokeEnergyMeth1 = as.numeric(Eac1),CokeEnergyMeth2= as.numeric(Eac2),
                  Ratio=RATIO,DA = DA,Downtime = totrowsmissing,
									LastTime = lasttime, LastRead = lastread, DailyCons = DailyCons,
									 Yld = as.character(vals[1]),PR=as.character(vals[2]),
									 Gsi = as.character(vals[3]),Yld1 = Yld1, Yld2=Yld2,ExpAmt = ExpAmtFinal,
									IrradianceSource=vals[4],
									Gsi714 = as.character(vals[5]),
									PR1=PR1,
									 stringsAsFactors = F)
	  }
	}
	write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm1, filepathm2,filepathm3,writefilepath)
{
  dataread2 =read.table(filepathm1,header = T,sep="\t",stringsAsFactors=F) #its correct dont change
  dataread1 = read.table(filepathm2,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
  if(!(is.null(filepathm3)))
		dataread3 = read.table(filepathm3,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
	PR1=PR2=PR3=DSP1=DSP2=DSP3=gmod=ExpAmt=PercExp=IrrSrc=NA
	PR1 = as.numeric(dataread1[,9])
	DSP1 = as.numeric(dataread1[,10])
	PR2 = as.numeric(dataread2[,9])
	DSP2 = as.numeric(dataread2[,10])
	dt = as.character(dataread1[,6])
	da = as.character(dataread1[,5])
	EacCokeMeth1 = as.numeric(dataread2[,2])
	EacCokeMeth2 = as.numeric(dataread2[,3])
	IrrSrc=as.character(dataread2[,13])
	CokeLoadMeth1=ArtificialLoad=CokeLoadMeth2=PercentageArtSolar=RatioCokeLoad=NA
	ltCokeLoad=lrCokeLoad=DailyCons=NA
  if(!(is.null(filepathm3)))
	{
	CokeLoadMeth1 = as.numeric(dataread3[,2])
	CokeLoadMeth2 = as.numeric(dataread3[,3])
	ArtificialLoad = CokeLoadMeth1 + EacCokeMeth1
	PercentageArtSolar = round(((EacCokeMeth1*100) / (ArtificialLoad)),2)
	PR3 = as.numeric(dataread3[,10])
	DSP3 = as.numeric(dataread3[,11])
	gmod = as.numeric(dataread3[,12])
	ExpAmt = as.numeric(dataread3[,15])
	PercExp = round(ExpAmt*100/EacCokeMeth2,2)
	}
	SolarEnergyMeth1 = as.numeric(dataread1[,2])
	SolarEnergyMeth2 = as.numeric(dataread1[,3])
  RatioCoke = as.numeric(dataread2[,4])
  RatioSolar = as.numeric(dataread1[,4])
  if(!(is.null(filepathm3)))
		RatioCokeLoad = as.numeric(dataread3[,4])
	CABLOSS= (EacCokeMeth2/SolarEnergyMeth2)
	CABLOSS = abs(1 - CABLOSS)*100
	CABLOSSTOPRINT <<- format(round(CABLOSS,2),nsmall=2)
	CABLOSS = round(CABLOSS,3)
	SOLENERGYLOSS <<- format(round((SolarEnergyMeth2*100/(CokeLoadMeth2 + SolarEnergyMeth2)),1),nsmall=1)
	ltSol = as.character(dataread1[,7])
	lrSol = as.character(dataread1[,8])
	ltCoke = as.character(dataread2[,7])
	lrCoke = as.character(dataread2[,8])
  if(!(is.null(filepathm3)))
	{
	ltCokeLoad = as.character(dataread3[,7])
	lrCokeLoad = as.character(dataread3[,8])
	DailyCons = as.character(dataread3[,9])
  }
	df = data.frame(Date = substr(as.character(dataread1[1,1]),1,10),
	DA= da,
	Downtime = dt,
	EacCokeMeth1 = EacCokeMeth1,
	EacCokeMeth2 = EacCokeMeth2,
	SolarEnergyMeth1=SolarEnergyMeth1,
	SolarEnergyMeth2=SolarEnergyMeth2,
	RatioCoke=RatioCoke,
	RatioSolar=RatioSolar,
	CableLoss=CABLOSS,
	LastTimeSol=ltSol,
	LastReadSol=lrSol,
	LastTimeCoke=ltCoke,
	LastReadCoke=lrCoke,
	CokeLoadMeth1=CokeLoadMeth1,
	CokeLoadMeth2=CokeLoadMeth2,
	RatioCokeLoad=RatioCokeLoad,
	LastTimeCokeLoad=ltCokeLoad,
	LastReadCokeLoad=lrCokeLoad,
	ArtificialLoad=ArtificialLoad,
	PercentageSolar=PercentageArtSolar,
	DailyCons = DailyCons,
	Gmod = gmod,
	PRSolar = PR1,
	PRCoke = PR2,
	PRArtificial = PR3,
	DSPSolar = DSP1,
	DSPCoke = DSP2,
	DSPArtificial = DSP3,
	ExpAmt = ExpAmt,
	PercExp = PercExp,
	IrrSrc=IrrSrc,
	stringsAsFactors=F)
  {
    if(file.exists(writefilepath))
    {
      write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
}

