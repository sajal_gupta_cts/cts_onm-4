rm(list = ls())
errHandle = file('/home/admin/Logs/LogsIN013History.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

require('mailR')
source('/home/admin/CODE/IN013Digest/FTPIN013dump.R')
source('/home/admin/CODE/Send_mail/sendmail.R')

LTCUTOFF = .001
FIREERRATA = c(1,1)
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

timetomins = function(x)
{
  hrs = unlist(strsplit(x,":"))
  mins = as.numeric(hrs[2])
  hrs = as.numeric(hrs[1]) * 60
  return((((hrs + mins)/1)+1))
}
FIRETWILIONA=c(0,0)
checkErrata = function(row,ts,no)
{
  if(ts < 540 || ts > 960)
	{return()}
	if(FIREERRATA[no] == 0)
	{
	  print(paste('Errata mail already sent','so no more mails'))
		return()
	}
	{
	if(!is.finite(as.numeric(row[2])))
	{
		FIRETWILIONA[no] <<- FIRETWILIONA[no]+ 1
	}
	else
		FIRETWILIONA[no] <<- 0
	}
	if((FIRETWILIONA[no] > 5) || (as.numeric(row[2]) < LTCUTOFF && is.finite(as.numeric(row[2]))))
	{
		FIREERRATA[no] <<- 0
		subj = paste('IN-013X Meter-',no,' down',sep="")
    body = 'Last recorded timestamp and reading\n'
		body = paste(body,'\n\nTimestamp:',as.character(row[1]))
		body = paste(body,'\n\nReal Power Tot kW reading:',as.character(row[2]))
		if(FIRETWILIONA[no]>5)
		{
		body = paste(body,'\n\nMore than 5 continuous readings are NA')
		}
recipients = getRecipients("IN-013X","a")
		send.mail(from = 'operations@cleantechsolar.com',
							to = recipients,
							subject = subj,
							body = body,
							smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com',passwd = 'CTS&*(789', tls = TRUE),
							authenticate = TRUE,
							send = TRUE,
							debug = F)
		print(paste('Errata mail sent '))
		msgbody = paste(subj,"\n Timestamp:",as.character(row[1]),
										"\n Real Pow Tot kW reading:",as.character(row[2]))
		command = paste('python /home/admin/CODE/Send_mail/twilio_alert.py',' "',msgbody,'"',' "IN-013X"',sep = "")
		system(command)
		print('Twilio message fired')
		FIRETWILIONA <<-0
		recordTimeMaster("IN-013X","TwilioAlert",as.character(row[1]))
	}
	
}
stitchFile = function(path,days,pathwrite,erratacheck)
{
  x = 1
	t = try(read.csv(paste(path,days[x],sep="/"),header = T, skip=6,stringsAsFactors=F),silent = T)
  if(length(t)==1)
	{
		print(paste("Skipping",days[x],"Due to err in file"))
		return()
	}
  dataread = t
	if(nrow(dataread) < 1)
		return()
  dataread = dataread[,-c(1,2)]
  currcolnames = colnames(dataread)
	no = 1
	{
	if(grepl('Gsi00',days[x]))
	idxvals = length(colnames)-1
	else if(grepl('Tmod',days[x]))
	idxvals = length(colnames)
	else if (grepl('Meter 2',days[x]))
	{
		idxvals = match(currcolnames,colnames2)
		no = 2
	}
	else if (grepl('Meter 1',days[x]))
  idxvals = match(currcolnames,colnames)
  }
	for(y in 1 : nrow(dataread))
  {
	  ta = substr(as.character(dataread[y,1]),1,10)
		ta = format(as.Date(ta,'%m-%d-%Y'),'%Y-%m-%d')
		sp = unlist(strsplit(as.character(dataread[y,1])," "))
		dataread[y,1] = paste(ta,sp[2])
		idxts = timetomins(substr(as.character(dataread[y,1]),12,16))
    if(!is.finite(idxts))
    {
      print(paste('Skipping row',y,'as incorrect timestamp'))
      next
    }
		{
		if(no==1)
   	 rowtemp2 = rowtemp
    else if(no==2)
			rowtemp2 = rowtempm2
		}
		yr = substr(as.character(dataread[y,1]),1,4)
    pathwriteyr = paste(pathwrite,yr,sep="/")
    checkdir(pathwriteyr)
    mon = substr(as.character(dataread[y,1]),1,7)
    pathwritemon = paste(pathwriteyr,mon,sep="/")
    checkdir(pathwritemon)
		pathwritestation = paste(pathwritemon,"/Meter-",no,sep="")
		checkdir(pathwritestation)
    day = substr(as.character(dataread[y,1]),1,10)
    filename = paste(stnname," M",no,"] ",day,".txt",sep="")
    pathtowrite = paste(pathwritestation,filename,sep="/")
		{
		if(grepl("Gsi00",days[x]) || grepl('Tmod',days[x]))
		{
		  rowtemp2[idxvals] = as.numeric(dataread[y,2])
	 		rowtemp2[1] = as.character(dataread[y,1])
		}
		else
    rowtemp2[idxvals] = dataread[y,]
		}
		rowtemp2 = unlist(rowtemp2)
  {
    if(!file.exists(pathtowrite))
    {
			rowtemp2 =rbind(rowtemp2)
			print(rowtemp2)
      df = data.frame(rowtemp2,stringsAsFactors = F)
      {
			if(no==1)
			colnames(df) = colnames
      else if (no==2)
			colnames(df) = colnames2
			}
			if(idxts != 1)
        {
          df[idxts,] = rowtemp2
          {
					if(no==1)
					df[1,] = rbind(unlist(rowtemp))
					else if(no==2)
					df[1,] = rbind(unlist(rowtempm2))
        	}
				}
				FIREERRATA[no] <<- 1
				FIRETWILIONA[no] <<-0
    }
    else
    {
      	df = read.table(pathtowrite,header =T,sep = "\t",stringsAsFactors=F)
			{
				if(idxts > nrow(df))
				{
					df[idxts,]=rowtemp2
				}
				else
				{
					if(as.character(rowtemp2[1])==as.character(df[idxts,1]))
					{
						if(grepl("Gsi00",days[x]) || grepl("Tmod",days[x]))
						{
							 df[idxts,idxvals]=dataread[y,2]#dont replace with rowtemp2 this is correct
						}
						else
						df[idxts,(1:(length(rowtemp2)-2))] = rowtemp2[1:(length(rowtemp2)-2)]
					}
					else
					{
						print(paste('Mismatch Old time',as.character(df[idxts,1]),
						'New time',as.character(rowtemp2[1])))
						df[(idxts+1):(nrow(df)+1),] = df[idxts:nrow(df),]
						df[idxts,]=rowtemp2
					}
				}
      	#df[idxts,] = rowtemp2
			}
    }
  }
	if(erratacheck != 0 && (!grepl('Gsi00',days[x])) && (!grepl('Tmod',days[x])))
	{
	  pass = c(as.character(df[idxts,1]),as.character(as.numeric(df[idxts,15])/1000))
	  checkErrata(pass,idxts,no)
	}
	df = df[complete.cases(df[,1]),]
	tdx = as.POSIXct(strptime(as.character(df[,1]), "%Y-%m-%d %H:%M:%S"))
	df = df[order(tdx),]
  write.table(df,file = pathtowrite,row.names = F,col.names = T,sep = "\t",append = F)
}
	recordTimeMaster("IN-013X","Gen1",as.character(df[nrow(df),1]))
	print(paste('Modified',pathtowrite))
}

path = '/home/admin/Data/Episcript-CEC/EGX IN-013X FTP Dump'
checkdir(path)
pathwrite = '/home/admin/Dropbox/Gen 1 Data/[IN-013X]'
pathdatelastread = '/home/admin/Start'
checkdir(pathdatelastread)
pathdatelastread = paste(pathdatelastread,'IN013.txt',sep="/")
days = dir(path)
days = days[grepl("csv",days)]
#idxtostart = c(2,1,4,3)
idxtostart = c(1,1,1,1)
lastrecordeddate = c()
{
	if(!file.exists(pathdatelastread))
	{
	  print('Last date read file not found so cleansing system from start')
		system('rm -R "/home/admin/Dropbox/Gen 1 Data/[IN-013X]"')
		idxtostart = c(1,1,1,1)
	}
	else
	{
		lastrecordeddate = readLines(pathdatelastread)
		#lastrecordeddate = as.character(lastrecordeddate[1])
		days2 = days[grepl('Gsi00',days)]
		days3 = days[grepl('Tmod',days)]
		days1 = days[grepl('Meter 1',days)]
		days4 = days[grepl('Meter 2',days)]

		idxtostart[1] = match(lastrecordeddate[1],days1)
		idxtostart[2] = match(lastrecordeddate[2],days2)
		idxtostart[3] = match(lastrecordeddate[3],days3)
		idxtostart[4] = match(lastrecordeddate[4],days4)

		if(!is.finite(idxtostart[3]))
		idxtostart[3] = 1
		print(paste('Date read is',lastrecordeddate,'and idxtostart is',idxtostart))
	}
}
if(length(idxtostart) < 3)
idxtostart = c(idxtostart,1,1)

checkdir(pathwrite)
startidxnw = which(grepl("Meter 1",days) %in% TRUE)
startidxnw2 = which(grepl("Meter 2",days) %in% TRUE)
colnames = colnames(read.csv(paste(path,days[startidxnw[length(startidxnw)]],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnames = colnames[-c(1,2)]
colnames = c(colnames,"Gsi00","Tmod")
colnames2 = colnames(read.csv(paste(path,days[startidxnw2[length(startidxnw2)]],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnames2 = colnames2[-c(1,2)]

rowtemp = rep(NA,(length(colnames)))
rowtempm2 = rep(NA,(length(colnames2)))
x=1
stnname =  "[IN-013X"
{
	offset=0
	daysac = days
	days = daysac[grepl("Gsi00",daysac)]
	if(is.finite(idxtostart[2]))
	{
	if((idxtostart[2]-offset) <= length(days))
	{
		print(paste('idxtostart is',idxtostart[2],'while length of days is',length(days)))
		for(x in (idxtostart[2]-offset) : length(days))
		{
			stitchFile(path,days[x],pathwrite,0)
		}
		lastrecordeddate[2] = as.character(days[x])
	}
	}
	#offset = offset + length(days)
	days = daysac[grepl("Meter 1",daysac)]
	if((idxtostart[1]-offset) <= length(days))
	{
		print(paste('idxtostart is',idxtostart[1],'while length of days is',length(days)))
		for(x in (idxtostart[1]-offset) : length(days))
		{
			stitchFile(path,days[x],pathwrite,0)
		}
		lastrecordeddate[1] = as.character(days[x])
	}
	#offset = offset + length(days)
	days = daysac[grepl("Meter 2",daysac)]
	if(is.finite(idxtostart[4]))
	{
	if((idxtostart[4]-offset) <= length(days))
	{
		print(paste('idxtostart is',idxtostart[4],'while length of days is',length(days)))
		for(x in (idxtostart[4]-offset) : length(days))
		{
			stitchFile(path,days[x],pathwrite,0)
		}
		lastrecordeddate[4] = as.character(days[x])
	}
	}
	#offset = offset + length(days)
	days = daysac[grepl("Tmod",daysac)]
	if(is.finite(idxtostart[3]))
	{
	if((idxtostart[3]-offset) <= length(days))
	{
		print(paste('idxtostart is',idxtostart[3],'while length of days is',length(days)))
		for(x in (idxtostart[3]-offset) : length(days))
		{
			stitchFile(path,days[x],pathwrite,0)
		}
		lastrecordeddate[3] = as.character(days[x])
	}
	}
	else
	{
		print('No historical backlog')
	}
}
days = daysac
print('_________________________________________')
print('Historical Calculations done')
print('_________________________________________')
reprint = 1
write(lastrecordeddate,file =pathdatelastread)
firewarning = 0
while(1)
{
	print('Checking FTP for new data')
 	filefetch = dumpftp(days,path)
	if(filefetch == 0)
	{
		Sys.sleep(600)
		next
	}
	print('FTP Check complete')
  daysnew = dir(path)
	daysnew = daysnew[grepl("csv",daysnew)]
	if(length(daysnew) == length(days))
	{
		if(reprint == 1)
		{
			reprint = 0
			print('Waiting for new file dump')
		}
		Sys.sleep(600)
		next
	}
	reprint = 1
	match = match(days,daysnew)
	days = daysnew
	match = match[complete.cases(match)]
	print(length(match))
	daysnew = daysnew[-match]
	for(x in 1 : length(daysnew))
	{
		print(paste('Processing',daysnew[x]))
		stitchFile(path,daysnew[x],pathwrite,firewarning)
		print(paste('Done',daysnew[x]))
	}
	daysnew2 = daysnew[grepl('Gsi00',daysnew)]
	daysnew3 = daysnew[grepl('Tmod',daysnew)]
	daysnew = daysnew[grepl('Meter 1',daysnew)]
	daysnew4 = daysnew[grepl('Meter 2',daysnew)]
	if(length(daysnew))
	lastrecordeddate[1] = as.character(daysnew[length(daysnew)])
	if(length(daysnew2))
	lastrecordeddate[2] =as.character(daysnew2[length(daysnew2)])
	if(length(daysnew3))
	lastrecordeddate[3] = as.character(daysnew3[length(daysnew3)])
	if(length(daysnew4))
	lastrecordeddate[4] = as.character(daysnew4[length(daysnew4)])
	write(c(lastrecordeddate[1],
	lastrecordeddate[2],
	lastrecordeddate[3],lastrecordeddate[4]),pathdatelastread)
	gc()
	firewarning = 1
}
print('Exit Loop')
sink()
