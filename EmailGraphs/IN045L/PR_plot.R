plot_PR = function(currday) {
  
  require(lubridate)
  require(ggplot2)
  require(scales)
  require(dplyr)
  
  ###################declare constants######################
  #file paths
  ROOT_PATH = "/home/admin/Dropbox/Third Gen/[IN-045L]"
  SAVE_PATH = "/home/admin/Jason/cec intern/results/IN045L"
  GHI_PATH = "/home/admin/Dropbox/Third Gen/[IN-068L]/WMS_1"
  
  #site details
  SITE_NAME = "IN-045L"
  BUSINESS_PLAN_PR = 72.4
  SIZE = 133.92
  
  #graph items position constants
  XPOS_BIZ_PR = 1
  YPOS_BIZ_PR = 100
  XPOS_LABEL = 30
  YPOS_LABEL = 17.5
  XPOS_AVE = 30
  YPOS_AVE = 90
  
  #create directory if SAVE_PATH does not exist
  if(!dir.exists(SAVE_PATH)) {
    dir.create(SAVE_PATH)
  }
  
  ######################functions######################
  mov_day_ave = function(x,n=30){stats::filter(x,rep(1/n,n), sides=1)}
  
  ###########data cleaning + summarisation##################
  #get file from all directory
  yearFiles = list.files(path = ROOT_PATH, all.files = F, full.names = T, recursive = T)
  Inv1_Files = yearFiles[grepl("INVERTER_1", yearFiles)]
  Inv2_Files = yearFiles[grepl("INVERTER_2", yearFiles)]
  Inv3_Files = yearFiles[grepl("INVERTER_3", yearFiles)]
  yearFiles = yearFiles[grepl("MFM_1", yearFiles)]
  GHI_Files = list.files(path = GHI_PATH, all.files = F, full.names = T, recursive = T)
  data = read.csv(yearFiles[1], header=T, sep="\t", stringsAsFactors = F)
  
  #store all daily GSI, PR, and EAC values in final_data
  for (i in yearFiles[2:length(yearFiles)]) {
    temp_data = read.csv(i, header=T, sep="\t", stringsAsFactors = F)
    data = rbind(data, temp_data)
  }
  
  #getting files of inverters
  data_inv1 = read.csv(Inv1_Files[1], header=T, sep="\t", stringsAsFactors = F)
  data_inv2 = read.csv(Inv2_Files[1], header=T, sep="\t", stringsAsFactors = F)
  data_inv3 = read.csv(Inv3_Files[1], header=T, sep="\t", stringsAsFactors = F)
  
  for (i in 2:length(Inv1_Files)) {
    temp_data = read.csv(Inv1_Files[i], header=T, sep="\t", stringsAsFactors = F)
    data_inv1 = rbind(data_inv1, temp_data)
    temp_data = read.csv(Inv2_Files[i], header=T, sep="\t", stringsAsFactors = F)
    data_inv2 = rbind(data_inv2, temp_data)
    temp_data = read.csv(Inv3_Files[i], header=T, sep="\t", stringsAsFactors = F)
    data_inv3 = rbind(data_inv3, temp_data)
  }
  
  #change date to Date datatype
  data$Date = as.Date(data$Date)
  data$Mean_Yield = (data_inv1$Yld2 + data_inv2$Yld2 + data_inv3$Yld2)/3
  data$STD = sqrt(((data_inv1$Yld2 - data$Mean_Yield)^2+(data_inv2$Yld2 - data$Mean_Yield)^2+(data_inv3$Yld2 - data$Mean_Yield)^2)/3)
  data$COV = data$STD/data$Mean_Yield*100
  
  #Getting GSI files for missing PR values
  data_gsi = read.csv(GHI_Files[1], header=T, sep="\t", stringsAsFactors = F)
  
  for (i in GHI_Files[2:length(GHI_Files)]) {
    temp_data = read.csv(i, header=T, sep="\t", stringsAsFactors = F)
    data_gsi = rbind(data_gsi, temp_data)
  }
  
  data_gsi$Date = as.Date(data_gsi$Date)
  
  #fill in missing PR values
  for (i in data$Date[is.na(data$PR2)]) {
    if (is.element(i, data_gsi$Date)) {
      data[data$Date == i, "PR2"] = 100*data$Eac2[data$Date == i]/(data_gsi$GTI[data_gsi$Date == i]*SIZE)
    }
  }
  
  curr_day_idx = match(as.Date(currday), data$Date)
  data = data[1:curr_day_idx,]
  
  #removing bad PR values
  bad_data = filter(data, (PR2 > 100) | (PR2 <= 0))
  good_data = filter(data, (PR2 < 100) & (PR2 > 0))
  
  test = good_data[1:length(good_data$Date),]
  test$Move_Day_Ave_30 = mov_day_ave(test$PR2)
  
  #calculating PR average
  PR_ave = mean(test$PR2)
  
  ave_30_day = (sum(test$PR2[(length(test$PR2)-29):length(test$PR2)]))/30
  ave_60_day = (sum(test$PR2[(length(test$PR2)-59):length(test$PR2)]))/60
  ave_90_day = (sum(test$PR2[(length(test$PR2)-89):length(test$PR2)]))/90
  ave_lifetime = (sum(test$PR2[1:length(test$PR2)]))/(length(test$PR2))
  
  #text for plots
  data_duration = paste("From", test$Date[1],"to", test$Date[length(test$Date)])
  bizplan_PR_text = "Performance Ratio = 78.0% (business plan)"
  
  ave_30_text = paste("30-day Average:   ", sprintf("%0.1f", round(ave_30_day, digits=1)), "%", sep="")
  ave_60_text = paste("60-day Average:   ", sprintf("%0.1f", round(ave_60_day, digits=1)), "%", sep="")
  ave_90_text = paste("90-day Average:   ", sprintf("%0.1f", round(ave_90_day, digits=1)), "%", sep="")
  ave_lt_text = paste("Lifetime Average:  ", sprintf("%0.1f", round(ave_lifetime, digits=1)), "%", sep="")
  
  #highlight latest total PR value
  highlight_point = filter(test, Date==test$Date[length(test$Date)])
  highlight_point_COV = filter(data, Date==data$Date[length(data$Date)])
  
  #points for legend
  #legend_PR = data.frame(xname = test$Date[length(test$Date)-22], ypos = 10)
  #legend_COV = data.frame(xname = test$Date[length(test$Date)-22], ypos = 6)
  
  ################create the graph#################
  graph = ggplot(data=test) + 
    geom_point(aes(x=Date, y=PR2), colour="#63b8ff", shape=15) + 
    geom_point(data=highlight_point, aes(x=Date, y=PR2), colour="#00008b", shape=4) + 
    geom_text(data=highlight_point, aes(x=Date, y=PR2, label=sprintf("%0.1f", round(PR2, digits=1))), nudge_x=7, size=3, colour="#00008b") + 
    #geom_line(aes(x=Date, y=COV), colour="#ffa500") +
    geom_line(aes(x=Date, y=Move_Day_Ave_30), colour="#00008b", size=1) +
    geom_hline(yintercept=PR_ave, colour="#63b8ff") + 
    geom_hline(yintercept=BUSINESS_PLAN_PR, colour="#008000", linetype="dashed") + 
    theme_classic() + 
    ylim(0, 100) + 
    #scale_y_continuous(sec.axis=sec_axis(~./10, name="COV [%]"), limits=c(0,100)) + 
    theme(axis.title.x=element_blank(), plot.title = element_text(hjust = 0.5, face="bold"), plot.subtitle = element_text(hjust = 0.5, size=11), panel.border=element_rect(color="black", fill=NA, size=1)) + 
    labs(y="Performance Ratio [%]") + 
    scale_x_date(labels = date_format("%b/%y")) + 
    ggtitle(label=paste("[", SITE_NAME, "] Performance Ratios", sep=""), subtitle=data_duration) + 
    annotate(geom="text", x=as.Date(test$Date[XPOS_BIZ_PR]), y=YPOS_BIZ_PR, label=paste("Performance Ratio = ", BUSINESS_PLAN_PR, "% (business plan)", sep=""), hjust=0, color="#3A8A0C") +
    annotate(geom="text", x=as.Date(test$Date[XPOS_LABEL]), y=YPOS_LABEL, label=ave_30_text, hjust=0, color="#0C3A8A") +
    annotate(geom="text", x=as.Date(test$Date[XPOS_LABEL]), y=YPOS_LABEL-2.5, label=ave_60_text, hjust=0, color="#0C3A8A") +
    annotate(geom="text", x=as.Date(test$Date[XPOS_LABEL]), y=YPOS_LABEL-5, label=ave_90_text, hjust=0, color="#0C3A8A") +
    annotate(geom="text", x=as.Date(test$Date[XPOS_LABEL]), y=YPOS_LABEL-7.5, label=ave_lt_text, hjust=0, color="#0C3A8A") +
    annotate(geom="text", x=as.Date(test$Date[XPOS_AVE]), y=YPOS_AVE, label="30-day moving average (PR)", color="#00008b", size=3.5) + 
    annotate(geom="segment", x=as.Date(test$Date[XPOS_AVE+4]), xend=as.Date(test$Date[XPOS_AVE+4]), y=YPOS_AVE-2, yend=(test$Move_Day_Ave_30[34]+1), arrow=arrow(), color="#00008b", size=1.1, alpha=0.5) 
  #annotate(geom="text", x=as.Date(test$Date[length(test$Date)-20]), y=10, label="PR", hjust=0) +
  #annotate(geom="text", x=as.Date(test$Date[length(test$Date)-20]), y=6, label="COV", hjust=0) + 
  #geom_point(data=legend_PR, aes(x=xname, y=ypos), colour="#63b8ff", shape=15) +
  #geom_point(data=legend_COV, aes(x=xname, y=ypos), colour="#ffa500", shape=16) + 
  #annotate(geom="rect", xmin=test$Date[length(test$Date)-24], xmax=test$Date[length(test$Date)-12], ymin=4, ymax=12, fill=NA, color="black")
  graph
  
  #plot COV
  data_duration = paste("From", data$Date[1],"to", data$Date[length(data$Date)])
  
  graph_COV = ggplot(data=data, aes(x=Date, y=COV)) +
    geom_line(colour="#ffa500", size=1.25) +
    geom_point(data=highlight_point_COV, aes(x=Date, y=COV), colour="#ffa500", shape=4) + 
    geom_text(data=highlight_point_COV, aes(x=Date, y=COV, label=sprintf("%0.1f%%", round(COV, digits=1))), nudge_x=7, size=3, colour="#ffa500") + 
    labs(y="COV of Inverters [%]") +
    theme_classic() + 
    ylim(0, (max(data$COV[data$COV < 10]) + 1)) + 
    scale_x_date(labels = date_format("%b/%y")) + 
    theme(axis.title.x=element_blank(), plot.title = element_text(hjust = 0.5, face="bold"), plot.subtitle = element_text(hjust = 0.5, size=11), panel.border=element_rect(color="black", fill=NA, size=1)) +
    ggtitle(label=paste("[", SITE_NAME, "] ", "COV of Inverters", sep=""), subtitle=data_duration)
  graph_COV
  
  save_name = paste(SAVE_PATH,"/[", SITE_NAME, "] ", test$Date[length(test$Date)], ".pdf", sep="")
  
  #save to pdf
  pdf(save_name, width=11.69, height=8.27)
  print(graph)
  print(graph_COV)
  dev.off()
  
}