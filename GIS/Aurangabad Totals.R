rm(list = ls(all = T))
system('rm -R /home/admin/Dropbox/GIS/Summary')
system('rm -R /home/admin/Dropbox/GIS/Master')
system('mkdir /home/admin/Dropbox/GIS/Summary')
system('mkdir /home/admin/Dropbox/GIS/Master')
uniquecities = function(x)
{
  listofcities = c()
  cities = dir(x)
  idxcities = 1
  for(x in 1 : length(cities))
  {
    city = unlist(strsplit(cities[x],"_"))
    listofcities[idxcities] = city[5]
    listofcities = unique(listofcities)
    idxcities = length(listofcities) + 1
  }
  return(listofcities)
}

filltilts = function(cities,path,tilts)
{
  files = dir(path)
  idxrow = rep(1,length(cities))
  for(x in 1 : length(files))
  {
    city = unlist(strsplit(files[x],"_"))
		if(length(city) < 9){
      next
    }
    idxmatch = match(city[5],cities)
		{
		if(length(city) >= 10)
    tiltnew = paste(city[7],city[8])
		else
		tiltnew = paste(city[7])
		}
    tilts[[idxmatch]][idxrow[idxmatch]] = tiltnew
    tilts[[idxmatch]] = unique(tilts[[idxmatch]])
    idxrow[idxmatch] = length(tilts[[idxmatch]]) + 1    
  }
  return(tilts)
}

createdir = function(path,city)
{
  for(x in 1 : length(city))
  {
    if(!file.exists(paste(path,city[x],sep="/")))
    {
      dir.create(paste(path,city[x],sep="/"))
    }
  }
}

#source('/media/shravan/New Volume/CTSolar/Code/Bot/calcSummary.R')
source('/home/admin/CODE/GIS/calcSummary.R')
path = "/home/admin/Dropbox/GIS/Raw Files"
#path = "/media/shravan/New Volume/CTSolar/Aurangabad Data"
pathwrite = '/home/admin/Dropbox/GIS/Summary'
if(!file.exists(pathwrite))
{
dir.create(pathwrite)
}
#pathwrite = "/media/shravan/New Volume/CTSolar/GIS Summary"

cities = uniquecities(path)
createdir(pathwrite,cities)

files = dir(path)
y=1

tilts = vector('list',length(cities))
tilts = filltilts(cities,path,tilts)

tables = vector('list',length(cities))
for(x in 1 : length(cities))
{
  
    tables[[x]][1] = vector('list',1)
    tables[[x]][2] = vector('list',1)
    
    if(length(tilts[[x]]) < 1)
    {
      tables[[x]][3] = vector('list',1)
      next
    }
    for(y in 1 : length(tilts[[x]]))
    {
      tables[[x]][(y+2)] = vector('list',1)
    }
}

cumnames = paste(pathwrite,"/",cities,"/",toupper(substr(cities,1,9))," Aggregate.txt",sep="")
filenamestoattach = paste(toupper(substr(cities,1,9)),"Aggregate.txt")
y=2
for(y in 1 : length(files))
{
  dataReadInt = try(read.table(paste(path,files[y],sep="/"),header = T,sep=";"),silent = F)
	if(class(dataReadInt)=='try-error')
		next
	calcSummary(paste(path,files[y],sep="/"))
	print(files[y])
}
print('########################## CALLING AGGREGATE CHECK #######################')
checkAggregate(pathwrite)
computeMaster()
